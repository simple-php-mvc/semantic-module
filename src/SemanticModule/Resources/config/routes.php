<?php

use MVC\Server\Route;

return array(
    new Route(Route::$validMethods, '/semantic', 'SemanticModule\\Controller\\SemanticController::index', 'semantic'),
    new Route(Route::$validMethods, '/semantic/index', 'SemanticModule\\Controller\\SemanticController::index', 'semantic_index'),
    new Route(Route::$validMethods, '/semantic/home', 'SemanticModule\\Controller\\SemanticController::home', 'semantic_home'),
);