<?php

namespace SemanticModule\Controller;

use MVC\Controller\Controller,
    MVC\MVC;

/**
 * Semantic Controller
 * 
 * @author Ramón Serrano <ramon.calle.88@gmail.com>
 */
class SemanticController extends Controller
{

    function index(MVC $mvc)
    {
        return $mvc->getCvpp('twig')->render('Semantic/index.twig');
    }

    function home(MVC $mvc)
    {
        return $mvc->getCvpp('twig')->render('Semantic/home.twig');
    }
    
}
